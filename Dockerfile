FROM node:15
RUN mkdir -p /usr/src/
WORKDIR /usr/src/
COPY package.json .
RUN npm install
COPY . ./
# RUN npm install
ENV PORT 3000 
EXPOSE $PORT
CMD [ "node", "index.js" ]