const express = require('express')
const xmlparser = require("express-xml-bodyparser")
const xml = require('xml2js');
// const bodyParser = require('body-parser');

const app = new express()

const port = process.env.PORT || 3000

app.use(express.json())
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
// app.disable('x-powered-by');
// app.use(xmlparser())

// const bustHeaders = (request, response, next) => {
//   request.app.isXml = false;

//   if (request.headers['content-type'] === 'application/xml'
//     || request.headers['accept'] === 'application/xml'
//   ) {
//     request.app.isXml = true;
//   }
//   next();
// };

const xmlOptions = {
  // charkey: 'value',
  // trim: false,
  explicitRoot: false,
  explicitArray: false,
  // normalizeTags: false,
  // mergeAttrs: true,
};

// const builder = new xml.Builder({
//   renderOpts: { 'pretty': false }
// });

app.get('/', (req, res) => {
  res.send("<h1>Carlos Chavez, this tutorial is great!</h1>")
})

app.post('/client', xmlparser(xmlOptions), (req, res) => {
  console.log(req.body)
  console.log('content type:', req.headers['content-type'])
  const { username, email } = req.body
  
  const xmlString = new xml.Builder()
    .buildObject(
      {
        'User': {
          username, 
          email, 
          message:`request content type: ${req.headers['content-type']}`
        }
      })
  console.log(xmlString)
  res.send(xmlString)
})

app.listen(port, () => console.log(`listening on port ${port}`))